﻿using System.Text.Json.Serialization;

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable InconsistentNaming

namespace Core.Config;

public record Settings(
    [property: JsonPropertyName("SettingUi")]
    UiConfig UiConfig,
    [property: JsonPropertyName("SettingApi")]
    ApiConfig ApiConfig
);

public record UiConfig(
    DriverType DriverType,
    string ApplicationUrl,
    string[] Args,
    float Timeout,
    bool Headless,
    float SlowMo
);

public record ApiConfig(
    string ApiURL,
    bool IgnoreHTTPSErrors,
    float Timeout
);

public enum DriverType
{
    Chrome,
    Firefox,
    Edge,
    Chromium,
    Opera
}