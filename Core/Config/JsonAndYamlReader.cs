﻿using System.Text.Json;
using System.Text.Json.Serialization;
using YamlDotNet.Serialization;

namespace Core.Config;

public static class JsonAndYamlReader
{
    public static T ReadJson<T>(string path)
    {
        string file;
        //Получаем и считываем файл
        try
        {
            file = File.ReadAllText(path);
        }
        catch (FileNotFoundException e)
        {
            Console.WriteLine($"Файл не найден по пути {path}\n" + e);
            throw;
        }

        //Доблавляем игнорирование регистра
        var jsonSerializerSetting = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true,
               
        };

        //Добавляем конвертер enum в строку
        jsonSerializerSetting.Converters.Add(new JsonStringEnumConverter());

        //Deserializing the property of the Test setting class.
        return JsonSerializer.Deserialize<T>(file, jsonSerializerSetting) ?? 
               throw new InvalidOperationException($"Ошибка десерилизации {path}");
        
    }

    public static T ReadYaml<T>(string path)
    {
        var file = File.ReadAllText(path);
        var yamlDeserializer = new DeserializerBuilder().Build();

        return yamlDeserializer.Deserialize<T>(file);
    }
}