﻿// ReSharper disable ClassNeverInstantiated.Global

namespace Core.Config;

public record User
{
    public required string Username { get; set; }
    public required string Password { get; set; }
}