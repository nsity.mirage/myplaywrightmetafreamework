﻿using Core.Config;
using Microsoft.Playwright;

namespace Core.Driver;

public class ApiContextDriver : IDisposable, IApiContextDriver
{
    private readonly string _apiUrl;
    private readonly bool _ignoreHttpsErrors;
    private readonly float _timeout;

    private readonly Task<IAPIRequestContext> _requestContext;
    public ApiContextDriver(Settings settings)
    {
        _apiUrl = settings.ApiConfig.ApiURL;
        _ignoreHttpsErrors = settings.ApiConfig.IgnoreHTTPSErrors;
        _timeout = settings.ApiConfig.Timeout;
        _requestContext = CreateApiContext();
    }

    public IAPIRequestContext ApiRequestContext => _requestContext.GetAwaiter().GetResult();

    private async Task<IAPIRequestContext> CreateApiContext()
    {
        var playwright = await Playwright.CreateAsync();

        return await playwright.APIRequest.NewContextAsync(new APIRequestNewContextOptions
        {
            BaseURL = _apiUrl,
            IgnoreHTTPSErrors = _ignoreHttpsErrors,
            Timeout = _timeout * 1000
        });
    }
    
    public void Dispose()
    {
        _requestContext.Dispose();
        GC.SuppressFinalize(this);
    }
}