﻿using Core.Config;
using Microsoft.Playwright;

namespace Core.Driver
{
    public interface IPlaywrightWebDriverInitializer
    {
        Task<IBrowser> GetChromiumDriverAsync(Settings settings);
        Task<IBrowser> GetFirefoxDriverAsync(Settings settings);
        Task<IBrowser> GetChromeDriverAsync(Settings settings);
    }
}