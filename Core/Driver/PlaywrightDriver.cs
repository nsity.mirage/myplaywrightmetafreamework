﻿using Microsoft.Playwright;
using Core.Config;

namespace Core.Driver
{
    public class PlaywrightDriver : IDisposable, IPlaywrightDriver
    {
        private readonly Task<IBrowser> _browser;
        private readonly Task<IPage> _page;
        private readonly Settings _settings;
        private readonly IPlaywrightWebDriverInitializer _playwrightWebDriverInitializer;


        public PlaywrightDriver(Settings settings, IPlaywrightWebDriverInitializer playwrightWebDriverInitializer)
        {
            _settings = settings;
            _playwrightWebDriverInitializer = playwrightWebDriverInitializer;
            _browser = Task.Run(InitializePlaywright);
            _page = Task.Run(CreatePageAsync);
            Task.Run(NavigateToUrl);

        }
        public IPage Page => _page.Result;



        private async Task<IBrowser> InitializePlaywright()
        {
            return _settings.UiConfig.DriverType switch
            {
                DriverType.Chrome => await _playwrightWebDriverInitializer.GetChromeDriverAsync(_settings),
                DriverType.Firefox => await _playwrightWebDriverInitializer.GetFirefoxDriverAsync(_settings),
                _ => await _playwrightWebDriverInitializer.GetChromiumDriverAsync(_settings)
            };
        }

        public async Task<IPage> CreatePageAsync()
        {
            return await (await _browser).NewPageAsync();
        }

        public async Task NavigateToUrl()
        {
            await Page.GotoAsync(_settings.UiConfig.ApplicationUrl);
        }
        public async Task NavigateToUrl(string url)
        {
            await Page.GotoAsync(url);
        }

        public void Dispose()
        {
            _browser.Dispose();
        }
    }
}
