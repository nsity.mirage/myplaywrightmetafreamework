﻿using Microsoft.Playwright;

namespace Core.Driver;

public interface IApiContextDriver
{
    IAPIRequestContext ApiRequestContext { get; }

    void Dispose();
}