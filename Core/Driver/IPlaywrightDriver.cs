﻿using Microsoft.Playwright;

namespace Core.Driver;

public interface IPlaywrightDriver
{
    IPage Page { get; }

    Task<IPage> CreatePageAsync();
    Task NavigateToUrl();
    Task NavigateToUrl(string url);
    void Dispose();
}