﻿using Core.Config;
using Microsoft.Playwright;

namespace Core.Driver
{
    public class PlaywrightWebDriverInitializer : IPlaywrightWebDriverInitializer
    {
        private const float DefaultTimeout = 30f;

        public async Task<IBrowser> GetChromiumDriverAsync(Settings settings)
        {
            var options = GetParameters(settings.UiConfig.Timeout, settings.UiConfig.Headless, settings.UiConfig.SlowMo,settings.UiConfig.Args);
            options.Channel = "chromium";
            return await GetBrowserAsync(DriverType.Chromium, options);
        }

        public async Task<IBrowser> GetChromeDriverAsync(Settings settings)
        {
            var options = GetParameters(settings.UiConfig.Timeout, settings.UiConfig.Headless, settings.UiConfig.SlowMo,settings.UiConfig.Args);
            options.Channel = "chrome";
            return await GetBrowserAsync(DriverType.Chromium, options);
        }

        public async Task<IBrowser> GetFirefoxDriverAsync(Settings settings)
        {
            var options = GetParameters(settings.UiConfig.Timeout, settings.UiConfig.Headless, settings.UiConfig.SlowMo,settings.UiConfig.Args);
            options.Channel = "firefox";
            return await GetBrowserAsync(DriverType.Firefox, options);
        }


        private static async Task<IBrowser> GetBrowserAsync(DriverType driverType, BrowserTypeLaunchOptions options)
        {
            var playwright = await Playwright.CreateAsync();
            return await playwright[driverType.ToString().ToLower()].LaunchAsync(options);
        }



        private static BrowserTypeLaunchOptions GetParameters(float? timeout = DefaultTimeout, bool? headless = true, float? slowMo = null,params string[] args)
            => new()
            {
                Args = args,
                Timeout = ToMilliseconds(timeout),
                Headless = headless,
                SlowMo = slowMo,
            };

        private static float? ToMilliseconds(float? seconds)
        {
            return seconds * 1000;
        }
    }
}
