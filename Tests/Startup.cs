﻿using Core.Config;
using Core.Driver;
using GP.Components;
using GP.Components.Classes;
using Microsoft.Extensions.DependencyInjection;
using UI.Tests.Pages;
using static Core.Config.JsonAndYamlReader;

namespace UI.Tests;

public class Startup
{
    private readonly Settings _settings = ReadJson<Settings>("appsettings.json");
    private readonly IEnumerator<User> _users = ReadYaml<List<User>>("users.yaml").GetEnumerator();
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton(_settings);
        services.AddSingleton(_users);
        services.AddScoped<IPlaywrightDriver, PlaywrightDriver>();
        services.AddScoped<IApiContextDriver, ApiContextDriver>();
        services.AddScoped<IPlaywrightWebDriverInitializer, PlaywrightWebDriverInitializer>();
        services.AddScoped(typeof(LoginPage));
        services.AddScoped(typeof(ResultsRisPage));

    }
}