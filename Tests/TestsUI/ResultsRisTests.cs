﻿using Core.Driver;
using Data.Models;
using Microsoft.Playwright;
using UI.Tests.Pages;
using Settings = Core.Config.Settings;

namespace UI.Tests.TestsUI;

public class ResultsRisTests
{
    private readonly ResultsRisPage _resultsRisPage;
    private readonly IPage _page;

    public ResultsRisTests(IPlaywrightDriver playwrightDriver, Settings settings, ResultsRisPage resultsRisPage)
    {
        _resultsRisPage = resultsRisPage;
        playwrightDriver.NavigateToUrl(settings.UiConfig.ApplicationUrl + "results-register");
        _page = resultsRisPage.Page;
    }

    [Fact]
    public async Task CreateResultTest()
    {
        var result = new Result("Наименование результата", "Информационная система", "2023");
        await _resultsRisPage.CreateResultAsync(result);   
        await Assertions.Expect(_page.Locator(".ant-notification-notice")).ToHaveTextAsync("Данные успешно сохранены");
        
    }
    
    
}