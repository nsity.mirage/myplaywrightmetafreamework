﻿using Core.Driver;
using System.Text.Json;
using Core.Config;

namespace UI.Tests.TestsAPI;

public class TestsGp
{
    private readonly IApiContextDriver _contextDriver;
    private readonly IEnumerator<User> _users;

    public TestsGp(IApiContextDriver contextDriver, IEnumerator<User> users)
    {
        _contextDriver = contextDriver;
        _users = users;
    }

    [Fact]
    public async Task TestLogin()
    {
        var user = _users.Current;
        var response = await _contextDriver.ApiRequestContext.PostAsync($"api/Login/Login?login={user.Username}&password={user.Password}");
        var data = await response.JsonAsync();
        var sessionId = data?.Deserialize<Dictionary<string,object>>()?["sessionId"].ToString();
        Assert.True(sessionId is { Length: 32 });
    }
}