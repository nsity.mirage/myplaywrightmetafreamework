﻿using Core.Config;
using Core.Driver;
using Microsoft.Playwright;

namespace UI.Tests.Pages;

public class LoginPage
{
    public readonly IPage Page;
    private readonly User _user;

    public LoginPage(IPlaywrightDriver playwrightDriver, IReadOnlyList<User> users)
    {
        Page = playwrightDriver.Page;
        _user = users.GetEnumerator().Current;
    }


    public async Task LoginAsync() {
        await Page.GetByText("Профиль").FillAsync(_user.Username);
        await Page.GetByText("Пароль").FillAsync(_user.Password);
        await Page.GetByRole(AriaRole.Button, new() { Name = "Войти" }).ClickAsync();
    }
}