﻿using Data.Models;
using GP.Components.Classes;
using Microsoft.Playwright;

namespace UI.Tests.Pages;

public class ResultsRisPage
{
    public readonly IPage Page;
    private readonly LoginPage _loginPage;
    public readonly Menu Menu;

    public ResultsRisPage(LoginPage loginPage, Menu menu)
    {
        Page = loginPage.Page;
        _loginPage = loginPage;
        Menu = new Menu(Page); 
    }

    public async Task CreateResultAsync(Result result)
    {
        await _loginPage.LoginAsync();

        await Page.GetByRole(AriaRole.Button, new() { Name = "Добавить" }).ClickAsync();
        
        await Page.GetByLabel("Наименование результата").ClickAsync();
        
        await Page.GetByLabel("Наименование результата").FillAsync(result.Name);
        
        await Page.Locator(".plex-field-trigger").First.ClickAsync();
        
        await Page.Locator("input[name=\"\\32 8629458239\"]").CheckAsync();
        
        await Page.GetByRole(AriaRole.Button, new() { Name = "Выбрать" }).ClickAsync();
        
        await Page.Locator("div:nth-child(5) > div > .plex-field > .plex-field__input > .plex-field__triggers > .plex-field-trigger").ClickAsync();
        
        await Page.Locator("input[name=\"\\38 217266108\"]").CheckAsync();
        
        await Page.GetByRole(AriaRole.Button, new() { Name = "Выбрать" }).ClickAsync();
        
        await Page.Locator("div:nth-child(6) > div > .plex-field > .plex-field__input > .plex-field__triggers > .plex-field-trigger").ClickAsync();
        
        await Page.Locator("input[name=\"\\32 201955\"]").CheckAsync();
        
        await Page.GetByRole(AriaRole.Button, new() { Name = "Выбрать" }).ClickAsync();
        
        await Page.Locator("div:nth-child(2) > div > .plex-field > .plex-field__input > .plex-field__triggers > .plex-field-trigger").ClickAsync();
        
        await Page.Locator("input[name=\"\\35 156070\"]").CheckAsync();
        
        await Page.GetByRole(AriaRole.Button, new() { Name = "Выбрать" }).ClickAsync();
        
        await Page.GetByText("Общие параметры").ClickAsync();
        
        await Page.GetByLabel("Создание результата").GetByRole(AriaRole.Img).Nth(2).ClickAsync();
        
        await Page.GetByRole(AriaRole.Button, new() { Name = "Соглашение" }).ClickAsync();
        
        await Page.GetByLabel("Информационная система/источник").ClickAsync();
        
        await Page.GetByLabel("Информационная система/источник").FillAsync(result.SystemInformation);
        
        await Page.GetByText("Значениe результата").ClickAsync();
        
        await Page.GetByRole(AriaRole.Button, new() { Name = "Добавить" }).ClickAsync();
        
        await Page.GetByText("Добавить базовое значение").ClickAsync();
        
        await Page.GetByRole(AriaRole.Row, new() { Name = "Тысяча тонн –" }).GetByRole(AriaRole.Button).First.ClickAsync();
        
        await Page.GetByLabel("Создание результата").GetByRole(AriaRole.Textbox).FillAsync(Result.Year);
        
        await Page.GetByRole(AriaRole.Row, new() { Name = "Тысяча тонн –" }).GetByRole(AriaRole.Button).First.ClickAsync();
        
        await Page.GetByRole(AriaRole.Row, new() { Name = "2023 Тысяча тонн –" }).Locator("use").Nth(1).ClickAsync();

        await Page.RunAndWaitForResponseAsync(async () =>
        {
            await Page.GetByRole(AriaRole.Button, new() { Name = "Cохранить" }).ClickAsync();
        }, response => response.Ok);
        await Page.Locator(".ant-notification-notice").HoverAsync();
        await Page.ScreenshotAsync(new() {FullPage = true, Path = "../../../TestsUI/ScreenShots/Screen.jpg"});
        
        var url = Page.Url; //Todo: Добавить логирование id созданного результата
    }
}