﻿using Microsoft.Playwright;

namespace GP.Components.Interfaces;

public interface ITable
{
    ILocator GetCell(string rowName, int numberColumn);
}