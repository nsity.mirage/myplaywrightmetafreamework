﻿using Microsoft.Playwright;

namespace GP.Components.Interfaces;

public interface IRow
{
    List<ILocator> GetAllRows();
    
    ILocator GetFirstRow();
    ILocator GetLastRow();
    ILocator GetRow(string title);


}