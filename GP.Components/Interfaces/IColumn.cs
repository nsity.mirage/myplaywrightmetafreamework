﻿using Microsoft.Playwright;

namespace GP.Components.Interfaces;

public interface IColumn
{
    List<ILocator> GetAllColumns();
    
    ILocator GetFirstColumn();
    ILocator GetLastColumn();
    ILocator GetColumn(int numberColumn);

}