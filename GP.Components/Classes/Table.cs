﻿using Core.Driver;
using GP.Components.Interfaces;
using Microsoft.Playwright;

namespace GP.Components.Classes;

public class Table : ITable
{
    public readonly Column Column;
    public readonly Row Row;
    private readonly IPage _page;

    public Table(IPage page)
    {
        _page = page;
        Column = new Column(page);
        Row = new Row(page);
    }

    public ILocator GetCell(string rowName, int numberColumn) =>
        Row.GetRow(rowName).Locator(Column.GetColumn(numberColumn));
 
}