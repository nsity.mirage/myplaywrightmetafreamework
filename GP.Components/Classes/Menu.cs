﻿using Microsoft.Playwright;

namespace GP.Components.Classes;

public class Menu
{
    private readonly IPage _page;
    private ILocator MenuLocator => _page.Locator("aside").First;
    private ILocator Sider => MenuLocator.Locator("button");
    private ILocator MenuItems => MenuLocator.Locator("li");

    public Menu(IPage page)
    {
        _page = page;
    }

    public Task SiderClick() => Sider.ClickAsync();
    
    public ILocator SelectMenu(string title) => MenuItems.Filter(new() { HasText = title });
}