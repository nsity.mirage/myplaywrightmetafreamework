﻿using GP.Components.Interfaces;
using Microsoft.Playwright;

namespace GP.Components.Classes;

public class Row : IRow
{
    private ILocator RowsLocator => _page.Locator("tr");
    private readonly IPage _page;

    public Row(IPage page)
    {
        _page = page;
    }

    public List<ILocator> GetAllRows() => RowsLocator.AllAsync().Result.ToList();

    public ILocator GetFirstRow() => RowsLocator.First;

    public ILocator GetLastRow() => RowsLocator.Last;

    public ILocator GetRow(string title) => RowsLocator.Filter(new() { HasText = title });
}