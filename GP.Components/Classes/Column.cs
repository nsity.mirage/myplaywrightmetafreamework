﻿using GP.Components.Interfaces;
using Microsoft.Playwright;

namespace GP.Components.Classes;

public class Column : IColumn
{
    private ILocator ColumnsLocator => _page.Locator("td");
    private readonly IPage _page;

    public Column(IPage page)
    {
        _page = page;
    }

    public List<ILocator> GetAllColumns() => ColumnsLocator.AllAsync().Result.ToList();

    public ILocator GetFirstColumn() => ColumnsLocator.First;

    public ILocator GetLastColumn() => ColumnsLocator.Last;

    public ILocator GetColumn(int numberColumn) => ColumnsLocator.Nth(numberColumn);
}