﻿namespace Data.Models;

public class Result
{
     public Result(string name, string systemInformation, string year)
     {
          Name = name;
          SystemInformation = systemInformation;
          Year = year;
     }

     public string Name { get; set; }
     public string SystemInformation { get; set; }
     public static string Year { get; set; }
}
